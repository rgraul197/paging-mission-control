package fun.rich;

import org.apache.commons.collections4.multimap.HashSetValuedHashMap;

import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class DurationBasedMultiMapCache<K, V> {

    /**
     * The map of insert time based upon key
     */
    private final HashSetValuedHashMap<Long, K>  timeMap = new HashSetValuedHashMap<>();

    /**
     * Used to find the oldest time in cache
     */
    private final Stack<Long> timeList = new Stack<>();

    /**
     * The map of insert time based upon key
     */
    private final HashSetValuedHashMap<K, V> valueMap = new HashSetValuedHashMap<>();

    /**
     * The length of time before the record expires
     */
    private long expiryInMillis = 1000;
    /**
     * Default constructor
     */
    public DurationBasedMultiMapCache() {

    }

    /**
     * Time based constructor
     *
     * @param expiryInMillis the time to expire the value
     */
    public DurationBasedMultiMapCache(long expiryInMillis) {
        this.expiryInMillis = expiryInMillis;
    }

    public HashSetValuedHashMap<K, V> getCache() {
        return valueMap;
    }

    /**
     * Put a key value into cache
     *
     * @param key        the key
     * @param value      the value
     * @param insertTime the time of insert based upon object time
     * @return true iff pushed to cache
     */
    public boolean put(K key, V value, long insertTime) {
        timeMap.put(insertTime, key);
        timeList.push(insertTime);
        boolean returnVal = valueMap.put(key, value);
        boolean expireItems = true;
        while (expireItems) {
            Long timeCheck = timeList.get(0);
            if (timeCheck <= insertTime - expiryInMillis) {
                timeList.remove(0);
                Set<K> removeKey = timeMap.remove(timeCheck);
                if (null != removeKey && removeKey.size() > 0) {
                    Set<V> removeValue = valueMap.remove(removeKey.iterator().next());
                }
            } else {
                expireItems = false;
            }
        }
        return returnVal;
    }

    /**
     * Puts a map a key-values into cache
     *
     * @param m          the map of key-values
     * @param insertTime the associated time
     */
    public void putAll(Map<? extends K, ? extends V> m, long insertTime) {
        for (K key : m.keySet()) {
            put(key, m.get(key), insertTime);
        }
    }

    /**
     * Only puts if absent
     *
     * @param key        the key
     * @param value      the value
     * @param insertTime the object's time
     * @return the value
     */
    public boolean putIfAbsent(K key, V value, long insertTime) {
        if (valueMap.containsKey(key)) {
            return put(key, value, insertTime);
        } else {
            return false;
        }
    }

    /**
     * Clears the dependent structures.
     */
    public void clear() {
        valueMap.clear();
        timeList.clear();
        timeMap.clear();
    }
}
