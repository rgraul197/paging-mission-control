package run.rich


import fun.rich.DurationBasedMultiMapCache
import run.rich.AlertStatus.AlertStatus


/**
  * Class that inspects messages to see if an alert is needed.
  * Caches the messages for a known duration and checks if an alert should be fired.
  * @param duration the duration for keeping the message in the search window
  * @param frequency the number of issues found prior to sending an alert
  */
class Alerter(duration: Long, frequency: Int) {
  /**
    * A time based cache to only keep records for the supplied duration
    */
  val cache: DurationBasedMultiMapCache[String, TelemetryMessage] = new DurationBasedMultiMapCache(duration)

  /**
    * Process each message.  If 3 or more alerts occur for a single satellite raise an alert
    * @param message the message
    * @return the Alert if frequent enough null otherwise
    */
  def processMessage(message: TelemetryMessage): Alert = {
    cache.put(message.satelliteId, message, message.timestamp.getTime)
    val messages = cache.getCache.get(message.satelliteId)
    if (message.component.equals("BATT")) {
      val batteryLow = messages.stream.filter(_.component.equals("BATT")).filter(isAlert(_) == AlertStatus.RedLow).count
      if (batteryLow >= frequency) {
        return new Alert(message.satelliteId,  AlertStatus.RedLow, message.component, message.timestamp )
      }
    } else {
      val temperatureHigh = messages.stream.filter(_.component.equals("TSTAT")).filter(isAlert(_) == AlertStatus.RedHigh).count
      if (temperatureHigh >= frequency) {
        return new Alert(message.satelliteId,  AlertStatus.RedHigh, message.component, message.timestamp )
      }
    }
    null
  }

  /**
    * Use pattern matching to determine message status
    * @param message the message
    * @return the alert status
    */
  def isAlert(message: TelemetryMessage): AlertStatus = {
    message match {
      case TelemetryMessage(_, _, redHighLimit, _, _, _, rawValue, _) if rawValue > redHighLimit => AlertStatus.RedHigh
      case TelemetryMessage(_, _, _, _, _, redLowLimit, rawValue, _) if rawValue < redLowLimit => AlertStatus.RedLow
        //Note yellow not needed shown only for completeness.
//      case TelemetryMessage(_, satelliteId, _, yellowHighLimit, _, _, rawValue, _) if rawValue > yellowHighLimit => AlertStatus.YellowHigh
//      case TelemetryMessage(_, satelliteId, _, _, yellowLowLimit, _, rawValue, _) if rawValue < yellowLowLimit => AlertStatus.YellowHLow
      case _ => AlertStatus.Normal
    }
  }
}
