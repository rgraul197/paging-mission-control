package run.rich

import scala.io.Source

object Application  {
  def main(args: Array[String]): Unit = {
    var fileName = "./core/data/input.txt"
    if (args.length == 1) {
      fileName = args(1)
    }
    val currentDirectory = new java.io.File(".").getCanonicalPath
    println(currentDirectory)
    println("Hello, world!")
    val alerter: Alerter = new Alerter(5*60*1000, 3)
    val bufferedSource = Source.fromFile(fileName)
    print{"["}
    var printedRecord = false
    for (line <- bufferedSource.getLines) {
      val message:TelemetryMessage = TelemetryMessage(line)
      val alert = alerter.processMessage(message)
      if (null != alert) {
        if (printedRecord) {
          print(",")
        }
        println(Alert.toJson(alert))
        printedRecord = true
      }
      //      println(alert)
      //      cache.
      //      println(message)
    }

    print{"]"}
  }
}
