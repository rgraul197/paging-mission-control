package run.rich


import java.util.Date

//<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
import java.text.SimpleDateFormat
import run.rich.AlertStatus.AlertStatus

// Define a new enumeration with a type alias and work with the full set of enumerated values
/**
  * Enumeration for the different states for Alerts.
  */
object AlertStatus extends Enumeration {
  type AlertStatus = Value
  val RedHigh, RedLow, YellowHigh, YellowHLow, Normal = Value
}

/**
  * The information desired in the alert response
  * @param satelliteId the satellite id
  * @param severity the severity
  * @param component the component
  * @param timestamp the timestamp
  */
case class Alert(satelliteId: String, severity: AlertStatus, component: String, timestamp: Date) {}

/**
  * The Alert companion object to handle formatting output
  */
object Alert {
  /**
    * The output format for the date
    */
  val outputDateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  /**
    * The format of the Json output.  As this is a very simple structure no sense using Jackson and custom formatters
    */
  val template = "{\n    \"satelliteId\": %s,\n    \"severity\": \"%s\",\n    \"component\": \"%s\",\n    \"timestamp\": \"%s\"\n}"

  /**
    * Format the Alert as a json string
    * @param alert the alert
    * @return the json string
    */
  def toJson(alert : Alert) : String = {
    template.format(alert.satelliteId, formatSeverity(alert.severity), alert.component, outputDateFormat.format(alert.timestamp))
  }

  /**
    * Formats the Alert Status
    * @param status the enum
    * @return a string representation.
    */
  def formatSeverity(status : AlertStatus) : String = {
    status match {
      case AlertStatus.RedHigh => "RED HIGH"
      case AlertStatus.RedLow => "RED LOW"
    }
  }
}

/**
  * The telemetry message
  * @param timestamp the timestamp
  * @param satelliteId the satellite id
  * @param redHighLimit the red high limit
  * @param yellowHighLimit the yellow high limit
  * @param yellowLowLimit the yellow low limit
  * @param redLowLimit the red low limit
  * @param rawValue the raw value
  * @param component the component
  */
case class TelemetryMessage(timestamp: Date, satelliteId: String, redHighLimit: Int, yellowHighLimit: Int, yellowLowLimit: Int, redLowLimit: Int, rawValue: Double, component: String) {}

/**
  * Companion object to handle parsing
  */
object TelemetryMessage {
  /**
    * Input date formatter create it once and reuse it
    */
  val inputDateFormat: SimpleDateFormat = new SimpleDateFormat("yyyyMMdd hh:mm:ss.SSS")

  /**
    * Use the companion Object pattern to parse and type the message object
    *
    * @param message the string message
    * @return the TelemetryMessage class
    */
  def apply(message: String): TelemetryMessage = {
    val messageParts: Array[String] = message.split("\\|")
    new TelemetryMessage(parseTimeStamp(messageParts(0)), messageParts(1), messageParts(2).toInt, messageParts(3).toInt,
      messageParts(4).toInt, messageParts(5).toInt, messageParts(6).toDouble, messageParts(7))
  }

  /**
    * Convert string to a time stamp
    *
    * @param value the string
    * @return the date
    */
  def parseTimeStamp(value: String): Date = {
    inputDateFormat.parse(value)
  }

}
