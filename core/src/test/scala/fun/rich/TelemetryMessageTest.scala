package fun.rich

import java.text.SimpleDateFormat

import org.junit.Test
import run.rich.{Alert, Alerter, TelemetryMessage}

import scala.io.Source

class TelemetryMessageTest {

  @Test
  def test(): Unit = {
    val time = "20180101 23:01:05.001"
    val inputDateFormat: SimpleDateFormat = new SimpleDateFormat("yyyyMMdd hh:mm:ss.SSS")
    println(inputDateFormat.parse(time))
  }
  @Test
  def testParse() : Unit = {
      val input = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT"
      val message:TelemetryMessage = TelemetryMessage(input)
      println(message)
  }

  @Test
  def testReadRecords(): Unit = {
    //Store messages for 5 minutes
   val alerter: Alerter = new Alerter(5*60*1000, 3)
    val bufferedSource = Source.fromResource("input.txt")
    print{"["}
    var printedRecord = false
    for (line <- bufferedSource.getLines) {
      val message:TelemetryMessage = TelemetryMessage(line)
      val alert = alerter.processMessage(message)
      if (null != alert) {
        if (printedRecord) {
          print(",")
        }
        println(Alert.toJson(alert))
        printedRecord = true
      }
//      println(alert)
//      cache.
//      println(message)
    }

    print{"]"}
  }


}
